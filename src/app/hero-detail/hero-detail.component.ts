import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { Router } from 'express';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;

  meta_content: string;
  meta_image: string;
  meta_title: string;
  meta_href: string;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location,
  ) { }

  ngOnInit(): void {
    this.getHero();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(hero => {
        this.hero = hero;
        this.meta_title = this.hero.name;
        this.meta_href = `${environment.baseUrl}/details/${this.route.snapshot.params.id}`;
        this.meta_content = 'Prince of Persia is one of the most famous and entertaining games of all time. The game is a fantasy cinematic platformer, which takes place in Ancient Persia. ';
        this.meta_image = 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fmotociclismoonline.com.br%2Fnoticias%2Fducati-revela-preco-da-panigale-v4-na-italia%2F&psig=AOvVaw0BBPDMIApps1u8OxJoYHJP&ust=1599295367467000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLiTnaKOz-sCFQAAAAAdAAAAABAD';
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.heroService.updateHero(this.hero)
      .subscribe(() => this.goBack());
  }
}
