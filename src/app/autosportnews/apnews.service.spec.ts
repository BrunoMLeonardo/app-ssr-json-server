import { TestBed } from '@angular/core/testing';

import { ApnewsService } from './apnews.service';

describe('ApnewsService', () => {
  let service: ApnewsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApnewsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
