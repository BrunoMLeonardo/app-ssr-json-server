import { Component, OnInit } from '@angular/core';
import { ApnewsService } from './apnews.service';
import { AutoSportNews } from './AutoSportNews';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-autosportnews',
  templateUrl: './autosportnews.component.html',
  styleUrls: ['./autosportnews.component.css']
})
export class AutosportnewsComponent implements OnInit {

  apNews: AutoSportNews[];

  constructor(private apNewsService: ApnewsService,
    private title: Title,
    private meta: Meta) { }

  ngOnInit() {
    this.meta.updateTag({ name: 'description', content: 'Autosport news description meta tag' });
    this.title.setTitle('Autosport News');
    this.getNews();
  }

  getNews(): void {
    this.apNewsService.getAutosportNews()
      .subscribe(news => {
        this.apNews = news;
      });
  }

}
