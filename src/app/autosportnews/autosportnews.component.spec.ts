import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutosportnewsComponent } from './autosportnews.component';

describe('AutosportnewsComponent', () => {
  let component: AutosportnewsComponent;
  let fixture: ComponentFixture<AutosportnewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutosportnewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutosportnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
