import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MessageService } from '../message.service';
import { Observable, of } from 'rxjs';
import { Hero } from '../hero';
import { tap, catchError, map } from 'rxjs/operators';
import { AutoSportNews } from './AutoSportNews';

@Injectable({
  providedIn: 'root'
})
export class ApnewsService {
  private apNewsUrl = `${environment.apiUrl}/news`;  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET heroes from the server */
  getAutosportNews(): Observable<AutoSportNews[]> {
    return this.http.get<AutoSportNews[]>(this.apNewsUrl)
      .pipe(
        tap(_ => this.log('fetched ap news')),
        catchError(this.handleError<AutoSportNews[]>('getAutosportNews', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }

}
